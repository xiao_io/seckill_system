import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
// 导入组件库
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import * as Icons from '@element-plus/icons-vue'
import axios from 'axios'

axios.defaults.baseURL = 'http://52.82.14.53:8888/api/'

const app = createApp(App);
app.use(router).use(ElementPlus).mount('#app')
app.config.globalProperties.$http = axios

// 注册全局组件
Object.keys(Icons).forEach(key => {
    app.component(key, Icons[key as keyof typeof Icons])
})