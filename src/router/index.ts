import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    redirect: '/login',
  },
  {
    path: '/login',
    component: () => import('@/components/Login.vue')
  },
  {
    path: '/registered',
    component: () => import('@/components/registered.vue')
  },
  {
    path: '/home',
    redirect: '/goods',
    component: () => import('@/components/Shop.vue'),
    children: [
      { path: '/goods', component: () => import('@/components/Goods/goods.vue') },
      { path: '/user', component: () => import('@/components/User/user.vue') },
      { path: '/order', component: () => import('@/components/Order/order.vue') }
    ]
  },
  {
    path: '/admin',
    component: () => import('@/components/Admin/admin.vue'),
    redirect: '/admin/goods',
    children: [
      { path: '/admin/goods', component: () => import('@/components/Admin/Agoods.vue') },
      { path: '/admin/user', component: () => import('@/components/Admin/Auser.vue') },
      { path: '/admin/search', component: () => import('@/components/Admin/Asearch.vue') },
      { path: '/admin/promise', component: () => import('@/components/Admin/Apromise.vue') },
    ]
  }

  // component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

// 挂载路由导航守卫
router.beforeEach((to, from, next) => {
  if(to.path === '/login' || to.path === '/registered') return next()
  const tokenStr = window.sessionStorage.getItem('token')
  // console.log(tokenStr);
  if(!tokenStr) return next('/login')
  next()
})

export default router
