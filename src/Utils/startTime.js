export default function sTime(start) {
    // 返回毫秒数
    let nowTime = Date.now();
    let startTime = +new Date(start);
    // 1秒 = 1000毫秒
    let nowStart = (startTime - nowTime) / 1000;
    return parseInt(nowStart);
    // if (nowStart === 0) {
    //     return -1;
    // }

    // // 计算
    // let startSecond = parseInt(nowStart % 60);
    // startSecond = startSecond < 10 ? '0' + startSecond : startSecond;
    // let startMinute = parseInt(nowStart / 60 % 60);
    // startMinute = startMinute < 10 ? '0' + startMinute : startMinute;
    // let startHour = parseInt(nowStart / 60 / 60 % 24);
    // startHour = startHour < 10 ? '0' + startHour : startHour;
    // let startDay = parseInt(nowStart / 60 / 60 / 24);
    // startDay = startDay < 10 ? '0' + startDay : startDay;

    // return {
    //     startDay,
    //     startHour,
    //     startMinute,
    //     startSecond
    // }
}