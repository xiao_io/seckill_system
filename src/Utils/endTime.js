export default function eTime(start, end) {
    // console.log(start);
    // console.log(end);
    // 返回毫秒数
    let startTime = +new Date(start);
    let endTime = +new Date(end);
    // 1秒 = 1000毫秒
    let startEnd = (endTime - startTime) / 1000;
    return parseInt(startEnd);
    // if (startEnd === 0) {
    //     return -1;
    // }

    // // 计算
    // let endSecond = parseInt(nowStart % 60);
    // endSecond = endSecond < 10 ? '0' + endSecond : endSecond;
    // let endMinute = parseInt(nowStart / 60 % 60);
    // endMinute = endMinute < 10 ? '0' + endMinute : endMinute;
    // let endHour = parseInt(nowStart / 60 / 60 % 24);
    // endHour = endHour < 10 ? '0' + endHour : endHour;

    // return {
    //     endHour,
    //     endMinute,
    //     endSecond
    // }
}