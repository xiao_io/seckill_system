export default function now() {
    let time = new Date()
    // console.log(time);
    let y = time.getFullYear(),
        m = time.getMonth() + 1,
        d = time.getDate(),
        h = time.getHours(),
        min = time.getMinutes(),
        s = time.getSeconds();

    if (m < 10) m = '0' + m;
    if (d < 10) d = '0' + d;
    if (h < 10) h = '0' + h;
    if (min < 10) min = '0' + min;
    if (s < 10) s = '0' + s;
    // console.log(y + "-" + m + "-" + d + " " + h + ":" + min + ":" + s);
    return y + "-" + m + "-" + d + " " + h + ":" + min + ":" + s;
}